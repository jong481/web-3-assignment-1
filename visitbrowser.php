<?php
require_once('code/php/Navigation.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment 1x</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="css/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="css/material.css">
    <link rel="stylesheet" href="css/easy-autocomplete.css">
    <link rel="stylesheet" href="css/easy-autocomplete.theme.css">
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  </head>
  <body>
    <div id="loadModal" class="modalClass">
      <div id="loadSpinnerContainer">
        <div id="loadSpinner" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>    
      </div>
    </div>
    <div id="popup">
    </div>
    
    <input id="hiddenCountryCode" type="hidden" value="">

    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <?php
          outputHeader();
          outputNavigation();
        ?>
      </div>
      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
          <div class="demo-graphs mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--4-col">
            <div class="mdl-card__actions">
              <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">Filters</a>
            </div>
            <div id="filtersDiv" class="mdl-card__supporting-text mdl-color-text--grey-600">
                <!-- FILTERS HERE -->
                Country Name<br/>
                <input id="countryName"><br/>
                Device Type<br/>
                <select id="deviceType"></select><br/>
                Device Brand<br/>
                <select id="deviceBrand"></select><br/>
                Browser Name<br/>
                <select id="browserName"></select><br/>
                Referrer Name<br/>
                <select id="referrerName"></select><br/>
                OS Name<br/>
                <select id="osName"></select><br/>
            </div>
          </div>
          <div class="demo-graphs mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--8-col">
            <div class="mdl-card__actions">
              <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">Visits</a>
            </div>
            <div class="mdl-card__supporting-text mdl-color-text--grey-600">
                <table id="visitTable" class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Visit Date</th>
                      <th>Visit Time</th>
                      <th>IP Address</th>
                      <th>Country</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </main>
    </div>
    <script src="https://code.getmdl.io/1.1.1/material.min.js"></script>
    <script src="code/js/jquery.easy-autocomplete.js"></script>
    <script src="code/js/jquery.bpopup.min.js"></script>
    <script src="code/js/VisitBrowserFunctions.js"></script>
  </body>
</html>