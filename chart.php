<?php
require_once('code/php/Navigation.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment 1x</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="css/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="css/material.css">
  </head>
  <body>
    <div id="loadModal" class="modalClass">
      <div id="loadSpinnerContainer">
        <div id="loadSpinner" class="mdl-spinner mdl-spinner--single-color mdl-js-spinner is-active"></div>    
      </div>
    </div>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <?php
          outputHeader();
          outputNavigation();
        ?>
      </div>
      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
          <div class="demo-cards mdl-cell mdl-cell--4-col mdl-cell--8-col-tablet mdl-grid mdl-grid--no-spacing">
            <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop">
              <div class="mdl-card__actions mdl-card--border">
                  <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">Visits per Month</a>
              </div>
              <div class=" mdl-card--expand mdl-color--teal-300">
                Month:<select id='monthSelectAreaChart'></select>
                <div id="area_div"></div>
              </div>
            </div>
          
            <div class="demo-separator mdl-cell--1-col"></div>
            
            <div class="demo-updates mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--4-col-tablet mdl-cell--12-col-desktop">
              <div class="mdl-card__actions mdl-card--border">
                <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">Visits in a Month</a>
              </div>
              <div class=" mdl-card--expand mdl-color--blue-300">
                Month:<select id='monthSelectGeoChart'></select>
                <div id="geo_div"></div>
              </div>
            </div>
          </div>
          <div class="demo-graphs mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--8-col">
            <div class="mdl-card__actions">
              <a href="#" class="mdl-button mdl-js-button mdl-js-ripple-effect">Visits Chart</a>
            </div>
            <div class="mdl-card__supporting-text mdl-color-text--grey-600">
              Country 1: 
              <select id="countrySelect1"></select>
              <br/>Country 2: 
              <select id="countrySelect2"></select>
              <br/>Country 3: 
              <select id="countrySelect3"></select>
              <br/><input id="chartIt" type="button" name="chartIt" value="Chart It!" disabled/>
              <input id="flipIt" type="button" name="flipIt" value="Flip It!" disabled/>
            </div>
            <div id="bar_div"></div>
          </div>
        </div>
      </main>
    </div>
    <script src="https://code.getmdl.io/1.1.1/material.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="code/js/ChartFunctions.js"></script>
  </body>
</html>