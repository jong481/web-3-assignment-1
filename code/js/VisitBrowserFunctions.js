document.addEventListener("load", initialialOperations());

/*
Function is called when the page loads
*/
function initialialOperations()
{
    initializeFilters();
    initializeListeners();
    refreshDataTable();
}

/*
Function creates an event function ("Eventlistener") for #filtersDiv and its "select" options
*/
function initializeListeners()
{
    var selectArray = document.querySelectorAll("#filtersDiv select");
    for (var i=0; i<selectArray.length;i++)
    {
        selectArray[i].addEventListener("change", refreshDataTable);
    }
}

/*
Function creates an on click event listener for .imageLink 
*/
function initializeImageLinkListeners()
{
    var imageLinksArray = document.querySelectorAll(".imageLink");
    
    for (var i=0; i<imageLinksArray.length;i++)
    {
        imageLinksArray[i].addEventListener("click", function(event){displayRowData(event.target.id);});
    }
}

/*
Function uses the image spinner for once the function is invoked
this also creates a modal box asynchronously with information on device data such as:
IP Address  Device Type
Visit Date  Device Brand
Visit Time  Referrer Name
Country     OS Name
Browser
*/
function displayRowData(elementID)
{
    var loadSpinner = document.getElementById('loadModal');
    loadSpinner.style.display = 'block';
    
    $.ajax({
        url: "code/webservices/serviceVisits.php?rowID=" + elementID,
        async: false,
        success: function(deviceData){
           //referrer name, and operating system name
            var textIPAddress = "ip address: \t" + deviceData[0].ip_address + "<br/>";
            var textVisitDate = "visit date: \t" + deviceData[0].visit_date.toString().substring(0, deviceData[0].visit_date.toString().length - 9)  + "<br/>";
            var textVisitTime = "visit time: \t" + deviceData[0].visit_time.toString().substring(10, deviceData[0].visit_time.toString().length)  + "<br/>";
            var textCountry = "country: \t" + getCountryFromCountryCode(deviceData[0].country_code) + "<br/>"; 
            var textBrowser = "browser: \t" + getBrowserFromBrowserID(deviceData[0].browser_id) + "<br/>"; 
            var textDeviceType = "device type: \t" + getDeviceTypeFromDeviceTypeID(deviceData[0].device_type_id) + "<br/>";
            var textDeviceBrand = "device brand: \t" + getDeviceBrandFromDeviceBrandID(deviceData[0].device_brand_id) + "<br/>";
            var textReferrerName = "referrer: \t" + getReferrerNameFromReferrerID(deviceData[0].referrer_id) + "<br/>";
            var textOSName = "os: \t" + getOSNameFromOSID(deviceData[0].os_id) + "<br/>";
            
            var closeButton = '<a class="b-close">x<a/>';
            var modalText = closeButton + textIPAddress + textVisitDate + textVisitTime + textCountry + textBrowser + textDeviceType + textDeviceBrand + textReferrerName + textOSName;
            $('#popup').html(modalText);
        },
        complete: function(event, status){
            loadSpinner.style.display = 'none';

            $('#popup').bPopup(
            {
                position:[('auto'),('auto')]
            });
        }
    });
}

/*
Function calls returns the referrerID as a query string 
"code/webservices/serviceReferrers.php?referrerID="
*/
function getReferrerNameFromReferrerID(referrerID)
{
    var referrerName = "";
    
    $.ajax({
        url: "code/webservices/serviceReferrers.php?referrerID=" + referrerID,
        async: false,
        success: function(referrerData){
            referrerName = referrerData[0].value;   
        }
    });
    
    return referrerName;
}


/*
Function calls returns the osID as a query string 
"code/webservices/serviceOS.php?os_id="
*/
function getOSNameFromOSID(osID)
{
    var osName = "";
    
    $.ajax({
        url: "code/webservices/serviceOS.php?os_id=" + osID,
        async: false,
        success: function(osData){
            osName = osData[0].value;   
        }
    });
    
    return osName;
}

/*
Function calls returns the deviceBrandID as a query string 
"code/webservices/serviceBrands.php?deviceBrandID="
*/
function getDeviceBrandFromDeviceBrandID(deviceBrandID)
{
    var deviceBrand = "";
    
    $.ajax({
        url: "code/webservices/serviceBrands.php?deviceBrandID=" + deviceBrandID,
        async: false,
        success: function(deviceBrandData){
            deviceBrand = deviceBrandData[0].value;   
        }
    });
    
    return deviceBrand;
}


/*
Function calls returns the deviceTypeID as a query string 
"code/webservices/serviceDevices.php?deviceTypeID="
*/
function getDeviceTypeFromDeviceTypeID(deviceTypeID)
{
    var deviceType = "";
    
    $.ajax({
        url: "code/webservices/serviceDevices.php?deviceTypeID=" + deviceTypeID,
        async: false,
        success: function(deviceTypeData){
            deviceType = deviceTypeData[0].value;   
        }
    });
    
    return deviceType;
}

/*
Function calls returns the browserID as a query string 
"code/webservices/serviceBrowsers.php?browserID="
*/
function getBrowserFromBrowserID(browserID)
{
    var browser = "";
    
    $.ajax({
        url: "code/webservices/serviceBrowsers.php?browserID=" + browserID,
        async: false,
        success: function(browserData){
            browser = browserData[0].value;   
        }
    });
    
    return browser;
}

/*
Function calls returns the countryCode as a query string 
"code/webservices/serviceCountries.php?ISO="
*/
function getCountryFromCountryCode(countryCode)
{
    var country = "";
    
    $.ajax({
        url: "code/webservices/serviceCountries.php?ISO=" + countryCode,
        async: false,
        success: function(countryData){
            country = countryData[0].value;   
        }
    });
    
    return country;
}

/*
Function generates a query string for 
*/
function generateQueryString()
{
    //alert(elementDeviceType.options[elementDeviceType.selectedIndex].value);
    //alert(elementDeviceType.options[elementDeviceType.selectedIndex].text);
    var queryString = "";

    var elementCountryName = document.getElementById('countryName');
    var elementCountryValue = document.getElementById('hiddenCountryCode');
    var elementDeviceType = document.getElementById('deviceType');
    var elementDeviceBrand = document.getElementById('deviceBrand');
    var elementBrowserName = document.getElementById('browserName');
    var elementReferrerName = document.getElementById('referrerName');
    var elementOSName = document.getElementById('osName');
    
    if(elementCountryName.value != "")
    {
        queryString = queryString + "countryName=" + elementCountryValue.value + "&";
    }
    if(elementDeviceType.value != 0)
    {
        queryString = queryString + "deviceType=" + elementDeviceType.value + "&";
    }
    if(elementDeviceBrand.value != 0)
    {
        queryString = queryString + "deviceBrand=" + elementDeviceBrand.value + "&";
    }
    if(elementBrowserName.value != 0)
    {
        queryString = queryString + "browserName=" + elementBrowserName.value + "&";
    }
    if(elementReferrerName.value != 0)
    {
        queryString = queryString + "referrerName=" + elementReferrerName.value + "&";
    }
    if(elementOSName.value != 0)
    {
        queryString = queryString + "osName=" + elementOSName.value + "&";
    }

    if (queryString != "")
    {
        queryString = queryString.substr(0, queryString.length - 1);
        queryString = "?" + queryString;
    }
    
    return queryString;
}

/*
Function updates the Data Table based on the countryISO and a querystring is created 
with the new countryISO
*/
function refreshDataTable(countryISO)
{
    var loadSpinner = document.getElementById('loadModal');
    loadSpinner.style.display = 'block';
    
    var visitTableBody = document.querySelector("#visitTable tbody");  
    visitTableBody.innerHTML = "";

    var queryString = generateQueryString(countryISO);
    
    $.ajax({
        url: "code/webservices/serviceVisitBrowser.php" + queryString,
        async: true,
        success: function(visitData){

            for (var i=0; i<visitData.length; i++)
            {
                var trElement = document.createElement('tr');
                
                var tdElementLink = document.createElement('td');
                var imageLink = document.createElement('img');
                imageLink.setAttribute('id', visitData[i].id);
                imageLink.setAttribute('class', 'imageLink');
                imageLink.setAttribute('src', "images/more.png");
                imageLink.setAttribute('height', 10);
                
                var tdElementDate = document.createElement('td');
                var textNodeDate = document.createTextNode(visitData[i].visit_date);
                textNodeDate.textContent = textNodeDate.textContent.substring(0, textNodeDate.textContent.length - 9);
                var tdElementTime = document.createElement('td');
                var textNodeTime = document.createTextNode(visitData[i].visit_time);
                textNodeTime.textContent = textNodeTime.textContent.substring(10, textNodeTime.textContent.length);
                var tdElementIP = document.createElement('td');
                var textNodeIP = document.createTextNode(visitData[i].ip_address);
                var tdElementOS = document.createElement('td');
                var textNodeOS = document.createTextNode(visitData[i].country_code);
                
                tdElementLink.appendChild(imageLink);
                tdElementDate.appendChild(textNodeDate);
                tdElementTime.appendChild(textNodeTime);
                tdElementIP.appendChild(textNodeIP);
                tdElementOS.appendChild(textNodeOS);
                
                trElement.appendChild(tdElementLink);
                trElement.appendChild(tdElementDate);
                trElement.appendChild(tdElementTime);
                trElement.appendChild(tdElementIP);
                trElement.appendChild(tdElementOS);
                
                visitTableBody.appendChild(trElement);
            }
        },
        complete: function(event, status){
            initializeImageLinkListeners();
            loadSpinner.style.display = 'none';
        }
    });
}

/*
Function initilazes filters
*/
function initializeFilters()
{
    loadAutoComplete();
    loadDropdownData("deviceType", "Select a Device", "code/webservices/serviceDevices.php");
    loadDropdownData("deviceBrand", "Select a Brand", "code/webservices/serviceBrands.php");
    loadDropdownData("browserName", "Select a Browser", "code/webservices/serviceBrowsers.php");
    loadDropdownData("referrerName", "Select a Referrer", "code/webservices/serviceReferrers.php");
    loadDropdownData("osName", "Select an OS", "code/webservices/serviceOS.php");
}

/*
Function populates the dropdown not asynchronously:
selectElementID = the element that will be populated
defaultText = default text for the text node
serviceURL = the page that the request will be sent to
*/
function loadDropdownData(selectElementID, defaultText, serviceURL)
{
    var loadSpinner = document.getElementById('loadModal');
    loadSpinner.style.display = 'block';
    
    var selectElement = document.getElementById(selectElementID);
    var defaultOption = document.createElement('option');
    defaultOption.setAttribute('value', 0);
    var defaultTextNode = document.createTextNode(defaultText);
    
    defaultOption.appendChild(defaultTextNode);
    selectElement.appendChild(defaultOption);
    
    $.ajax({
        url: serviceURL,
        async: false,
        success: function(deviceData){
            for (var i=0; i<deviceData.length; i++)
            {
                var newOption = document.createElement('option');
                newOption.setAttribute('value', deviceData[i].id);
                var newTextNode = document.createTextNode(deviceData[i].value);
                
                newOption.appendChild(newTextNode);
                selectElement.appendChild(newOption);
            }
        },
        complete: function(event, status){
            loadSpinner.style.display = 'none';
        }
    });
}

function loadAutoComplete()
{
    var options = {
        url: "code/webservices/serviceCountries.php?general=return_all",
        getValue: "value",
        list: {	
            match: {
                enabled: true
            },
            onChooseEvent: function() 
            {
                var countryISO = $("#countryName").getSelectedItemData().id;
                $("#hiddenCountryCode").val(countryISO);
			    refreshDataTable();
		    },
        },
        
        theme: "square"
    };
    
    $("#countryName").easyAutocomplete(options);
}