function showCountriesTable() 
{
    var continent = 'table';
    continent += document.getElementById("continents").value;
  
    var tables = document.getElementsByClassName('countryTable');
    for(var i = 0; i < tables.length; i++)
    {
    tables[i].style.display = 'none';
    }
 
    document.getElementById(continent).style.display = 'block';
}

function showCount() 
{
    var deviceBrand = 'deviceBrand'; 
    deviceBrand += document.getElementById("deviceBrand").value;
    var parent = document.getElementById("deviceBrandCount");
    
    var childNodes = parent.childNodes;
    for(var x = 0; x < childNodes.length; x++)
    {
      childNodes[x].style.display = 'none';
    }
    
    document.getElementById(deviceBrand).style.display = 'block';
}