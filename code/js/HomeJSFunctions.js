document.addEventListener("load", initialialOperations());

function initialialOperations()
{
    addDocumentEventListeners();
    outputBrands();
    outputVisitsPerBrowser();
    outputContinents();
}

/*
Generates event listeners for different elements.
*/
function addDocumentEventListeners()
{
    var brandSelectElement = document.getElementById('deviceBrands');
    brandSelectElement.addEventListener("change", refreshVisitsPerBrand);
    
    var continentSelectElement = document.getElementById('continentList');
    continentSelectElement.addEventListener("change", refreshVisitsPerContinent);
}

/*
Refreshes the country visit list when the continent selection is changed.
*/
function refreshVisitsPerContinent()
{
    var selectedElement = document.getElementById('continentList');
    var continentTable = document.getElementById('continent_table');
    var continentTableBody = document.querySelector('#continent_table tbody');
    
    var loadSpinner = document.getElementById('loadModal');

    if (selectedElement.value != 0)
    {
        loadSpinner.style.display = 'block';
    
        continentTableBody.innerHTML = '';
        
        $.ajax({
            url: "code/webservices/serviceCountries.php?continent=" + selectedElement.value,
            async: true,
            success: function(countryData){
                for (var i=0; i<countryData.length; i++)
                {
                    $.ajax({
                        url: "code/webservices/serviceVisits.php?country_code=" + countryData[i].id,
                        async: false,
                        success: function(visitData){
                            var trElement = document.createElement('tr');
                            var tdElementCountryName = document.createElement('td');
                            var countryTextNode = document.createTextNode(countryData[i].value);
                            tdElementCountryName.appendChild(countryTextNode);
                            var tdElementVisitCount = document.createElement('td');
                            var visitCountTextNode = document.createTextNode(visitData.length);
                            tdElementVisitCount.appendChild(visitCountTextNode);
                            
                            trElement.appendChild(tdElementCountryName);
                            trElement.appendChild(tdElementVisitCount);
                            
                            continentTableBody.appendChild(trElement);
                        }
                    });
                }
            },
            complete: function (event, status){
                continentTable.style.display = 'block';
                loadSpinner.style.display = 'none';
            }
        });
    }
    else
    {
        continentTableBody.innerHTML = '';
    }
}

/*
Refreshes the visits per brand area when the brand selection is changed.
*/
function refreshVisitsPerBrand()
{
    var selectedElement = document.getElementById('deviceBrands');

    if (selectedElement.value != 0)
    {    
        var loadSpinner = document.getElementById('loadModal');
        loadSpinner.style.display = 'block';
    
        $.ajax({
          url: "/code/webservices/serviceVisits.php?general=brandCount&brand_id=" + selectedElement.value,
          success: function(data){
            var visitsPerBrandResultDiv = document.getElementById('visitsPerBrandResult');
            var countText = document.createTextNode(data[0].id + " visitors using " + data[0].device_brand_id +  " devices");
            
            visitsPerBrandResultDiv.innerHTML = '';
            visitsPerBrandResultDiv.appendChild(countText);
          },
          complete: function(event, status) {
              loadSpinner.style.display = 'none';
          }
        });
    }
    else
    {
        var visitsPerBrandResultDiv = document.getElementById('visitsPerBrandResult');
        visitsPerBrandResultDiv.innerHTML = '';
    }
}

/*
Generates the browser visit percentage list
*/
function outputVisitsPerBrowser()
{
    var loadSpinner = document.getElementById('loadModal');
    loadSpinner.style.display = 'block';
    
    var browserTable = document.getElementById("browser_table");
    
    $.ajax({
        url: "/code/webservices/serviceVisits.php?general=count_all",
        success: function(data){
            var totalVisitCount = parseInt(data);
            
            $.ajax({
               url: "code/webservices/serviceBrowsers.php",
               async: false,
               success: function(browserList){
                    for (var i=0; i<browserList.length; i++)
                    {
                        var browserName = browserList[i].value;
                        $.ajax({
                            url: "code/webservices/serviceVisits.php?general=browserCount&browser_id=" + browserList[i].id,
                            async: false,
                            success: function(browserVisits){
                                var visitPercentage = (parseFloat(((browserVisits[0].id/totalVisitCount) * 100)).toFixed(2)).toString() + "%";
                                var trElement = document.createElement('tr');
                                var tdElementName = document.createElement('td');
                                tdElementName.className = "mdl-data-table__cell--non-numeric";
                                var browserNameText = document.createTextNode(browserName);
                                tdElementName.appendChild(browserNameText);
                                var tdElementPercent = document.createElement('td');
                                var browserPercentText = document.createTextNode(visitPercentage);
                                tdElementPercent.appendChild(browserPercentText);
                                
                                trElement.appendChild(tdElementName);
                                trElement.appendChild(tdElementPercent);
                                browserTable.appendChild(trElement);
                            }
                        });
                    }
               }
            });
            
            browserTable.style.display = 'block';
            loadSpinner.style.display = 'none';
        }
    });
}

/*
Generates the dropdown list of brands.
*/
function outputBrands()
{
    var brandSelectElement = document.getElementById('deviceBrands');

    $.get( "/code/webservices/serviceBrands.php", function( data ) {
        for (var i=0; i<data.length; i++)
        {
            var optionElement = document.createElement('option');
            optionElement.setAttribute('value', data[i].id);
            var brandText = document.createTextNode(data[i].value);

            optionElement.appendChild(brandText);
            brandSelectElement.appendChild(optionElement);
        }
    });
}

/*
Generates the dropdown list of continents.
*/
function outputContinents()
{
    var continentSelectElement = document.getElementById('continentList');
    
    $.get( "/code/webservices/serviceContinents.php", function( data ) {
        for (var i=0; i<data.length; i++)
        {
            var optionElement = document.createElement('option');
            optionElement.setAttribute('value', data[i].value);
            var brandText = document.createTextNode(data[i].name);

            optionElement.appendChild(brandText);
            continentSelectElement.appendChild(optionElement);
        }
    });
}