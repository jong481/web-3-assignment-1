/*global google*/

document.addEventListener("load", initialOperations());
var flip = 1;

function initialOperations()
{
    var loadSpinner = document.getElementById('loadModal');
    loadSpinner.style.display = 'block';
    
    loadGoogle();
    addDocumentEventListeners();
    outputMonths();
    outputCountries();
}

/*
Loads the Google package. Timeout needed to ensure the package loads before the charts are drawn.
*/
function loadGoogle()
{
    if(typeof google != 'undefined' && google && google.load)
    {
        google.load("visualization", "1", {packages:["corechart"],callback: drawInitialChart});
    }
    else
    {
        setTimeout(loadGoogle, 30);
    }
}

/*
Retrieves and generates data for the Bar chart.
*/
function generateChartData()
{
    var country1 = $("#countrySelect1").val();
    var country2 = $("#countrySelect2").val();
    var country3 = $("#countrySelect3").val();
    
    var countryArray = [country1, country2, country3];
    var monthArray = ['1', '5', '9'];
    var data = [];
    var headerArray;
    
    headerArray = ['Month'];
    for (var i=0; i<countryArray.length; i++)
    {
        var rowArray = [];
        rowArray.push(monthArray[i].toString());

        for (var j=0; j<monthArray.length; j++)
        {
            $.ajax({
                url: "code/webservices/serviceVisits.php?general=monthCountryCount&month=" + monthArray[j] + "&country_code=" + countryArray[i],
                async: false,
                success: function(visitData){
                    if (j==0)
                    {
                        headerArray.push((visitData[0].country_code).toString());
                    }
                    rowArray.push(parseInt(visitData[0].id));
                }
            });
        }
        
        data.push(rowArray);
    }
    data.push(headerArray);
    
    return data;
}

/*
Creates the data array for the bar chart.
*/
function perepareBarData(chartData)
{
    var countries = ["Month",chartData[3][1], chartData[3][2], chartData[3][3]];
    var janMonth = ["January", chartData[0][1], chartData[1][1], chartData[1][2]];
    var mayMonth = ["May",  chartData[0][2], chartData[1][2], chartData[2][2]];
    var septMonth = ["September", chartData[0][3], chartData[1][3], chartData[2][3]];

    var fixedChartData = [countries, janMonth, mayMonth, septMonth];
    
    return fixedChartData;
}

/*
Creates the data array for a flipped-axis bar chart.
*/
function flipBarData(chartData)
{
    var months = ["Country", "January", "May", "September"]
    
    var country1 = [chartData[3][1],chartData[0][1], chartData[0][2],chartData[0][3]];
    var country2 = [chartData[3][2],chartData[1][1], chartData[1][2],chartData[1][3]];
    var country3 = [chartData[3][3],chartData[2][1], chartData[2][2],chartData[2][3]];
    
    var flippedChartData = [months, country1, country2, country3];
    return flippedChartData;
}

/*
Draws the actual bar chart.
*/
function drawBarChart(chartData)
{
    var data = google.visualization.arrayToDataTable(chartData);

    var options = {
      title: 'Year 2016 ',
      hAxis: {title: 'Site Visits',  titleTextStyle: {color: '#333'}},
      vAxis: {minValue: 0,}
    };

    var chart = new google.visualization.ColumnChart(document.getElementById('bar_div'));
    chart.draw(data, options);
}

/*
Event handler for generating the bar chart.
*/
function generateBarChart(flip)
{
    var chartData = generateChartData();
    
    if(flip % 2 == 0)
    {
        drawBarChart(perepareBarData(chartData));
    }
    else
    {
        drawBarChart(flipBarData(chartData));
    }
}

/*
Enables the bar chart buttons when all three countries are selected.
*/
function checkButton()
{
    var select1 = $("#countrySelect1").val();
    var select2 = $("#countrySelect2").val();
    var select3 = $("#countrySelect3").val();
    
    var chartItButton = document.getElementById('chartIt');
    var flipItButton = document.getElementById('flipIt');

    if (select1 != 0 && select2 != 0 && select3 != 0)
    {
        chartItButton.disabled = false;
        flipItButton.disabled = false;
    }
    else
    {
        chartItButton.disabled = true;
    }
}

/*
Generates the dropdown list of countries.
*/
function outputCountries()
{
    var select1 = document.getElementById('countrySelect1');
    outputCountriesData(select1);
    var select2 = document.getElementById('countrySelect2');
    outputCountriesData(select2);
    var select3 = document.getElementById('countrySelect3');
    outputCountriesData(select3);
}

/*
Retrieves the list of countries.
*/
function outputCountriesData(selectedNode)
{
    $.ajax({
        url: "code/webservices/serviceCountries.php?general=top10",
        async: true,
        success: function(countryData){
            
            var defaultElement = document.createElement('option');
            defaultElement.setAttribute('value', 0);
            var defaultTextNode = document.createTextNode('Choose Country');
            
            defaultElement.appendChild(defaultTextNode);
            selectedNode.appendChild(defaultElement);
            
            for(var i=0; i<countryData.length; i++)
            {
                var optionElement = document.createElement('option');
                optionElement.setAttribute('value', countryData[i].id);
                var countryTextNode = document.createTextNode(countryData[i].value);
                
                optionElement.appendChild(countryTextNode);
                selectedNode.appendChild(optionElement);
            }
        }
    });
}

/*
Generates event listeners for different elements.
*/
function addDocumentEventListeners()
{
    $("#monthSelectAreaChart").change(refreshAreaChart);
    $("#monthSelectGeoChart").change(refreshGeoChart);
    $("#countrySelect1").change(checkButton);
    $("#countrySelect2").change(checkButton);
    $("#countrySelect3").change(checkButton);
}

function refreshAreaChart()
{
    var value = $("#monthSelectAreaChart").val();
    outputAreaChart(value);
}

function refreshGeoChart()
{
    var value = $("#monthSelectGeoChart").val();
    outputGeoChart(value);
}

function outputMonths() 
{
    var monthSelectAreaChart = document.getElementById('monthSelectAreaChart');
    outputMonthData(monthSelectAreaChart);
    
    var monthSelectGeoChart = document.getElementById('monthSelectGeoChart');
    outputMonthData(monthSelectGeoChart);
}

function outputMonthData(outputNode) {
    var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    for (var i = 0; i < months.length; i++) {

        var optionElement = document.createElement('option');
        optionElement.setAttribute('value', i+1);
        var monthText = document.createTextNode(months[i]);
        optionElement.appendChild(monthText);

        outputNode.appendChild(optionElement);
    }
}

function outputAreaChart(month) {

    var json = callWebService(createQueryString(month));
    var chartData = prepareAreaData(json);

    var data = google.visualization.arrayToDataTable(chartData);

    var options = {
        title: 'Monthly Visits',
        hAxis: {
            title: 'Day',
            titleTextStyle: {
                color: '#333'
            }
        },
        vAxis: {
            minValue: 0
        }
    };

    var areaChart = new google.visualization.AreaChart(document.getElementById('area_div'));

    areaChart.draw(data, options);
}

function outputGeoChart(month) {
    var json = callWebService(createQueryString(month));
    var chartData = prepareGeoData(json);

    var data = google.visualization.arrayToDataTable(chartData);

    var options = {};

    var geoChart = new google.visualization.GeoChart(document.getElementById('geo_div'));

    geoChart.draw(data, options);
}

function drawInitialChart() {
    outputAreaChart(1);
    outputGeoChart(1);
    var chartItButton = document.getElementById('chartIt');
    chartItButton.addEventListener("click", generateBarChart);
    
    var flipItButton = document.getElementById('flipIt');
    flipItButton.addEventListener("click", function(){
        flip++;
        generateBarChart(flip);
    });
    
    var loadSpinner = document.getElementById('loadModal');
    loadSpinner.style.display = 'none';
}

function createQueryString(month) {
    //generate webservice url

    var url = 'code/webservices/serviceVisits.php?month=';
    url += month;
    return url;
}

/*calls the webservice and returns data array*/
function callWebService(urlString) {
    var posts = $.ajax({
        type: 'GET',
        url: urlString,
        async: false,
        dataType: 'json',
        done: function(results) {

            JSON.parse(results);
            return results;
        },
        fail: function(jqXHR, textStatus, errorThrown) {
            console.log('Could not get posts, server response: ' + textStatus + ': ' + errorThrown);
        }
    }).responseJSON;
    return posts;
}

/*modifies and prepares data from webservice to chart format*/
function prepareAreaData(data) {

    var table = [];
    table[0] = ["Date", "Visits"];

    for (var i = 0; i < data.length; i++) {
        var visit = data[i];
        var date = visit.visit_date;
        var dateDay = date.substring(8, 10);
        dateDay = dateDay.replace(/^0+/, '');
        dateDay = parseInt(dateDay);

        if (table[dateDay] == null) {
            table[dateDay] = [dateDay, 1];
        }
        else {
            var count = table[dateDay][1];
            count++;
            table[dateDay][1] = count;
        }
    }
    return table;
}


/*modifies and prepares data from webservice to chart format*/
function prepareGeoData(data) {

    var url = 'code/webservices/serviceCountries.php?general=return_all';

    var countries = callWebService(url);

    var table = [];
    table[0] = ["Country", "Popularity"];

    for (var i = 0; i < data.length; i++) {
        var visit = data[i];
        var country = visit.country_code;
        var countryName = "";

        if (table[country] == null) {

            for (var y = 0; y < countries.length; y++) {
                if (country == countries[y].id) {
                    countryName = countries[y].value;
                }
            }

            table[country] = [countryName, 1];
        }
        else {
            var count = table[country][1];
            count++;
            table[country][1] = count;
        }
    }
    
    var chartData = [];

    for (var i in table) {
        chartData.push(table[i]);
    }
    
    return chartData;
}
