<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    if (isset($_GET['os_id']))
    {
        $whereCriteria = $_GET['os_id'];

        $osGate = new OperatingSystemsTableGateway($dbAdapter);
        $result = $osGate->findRecordsBy("ID", array($whereCriteria), true);
    }
    else
    {
        $osGate = new OperatingSystemsTableGateway($dbAdapter);
        $result = $osGate->findAll();
    }

    echo json_encode($result);
}
?>