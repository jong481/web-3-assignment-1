<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    if (isset($_GET['deviceTypeID']))
    {
        $whereCriteria = $_GET['deviceTypeID'];

        $deviceGate = new DeviceTypeTableGateway($dbAdapter);
        $result = $deviceGate->findRecordsBy("ID", array($whereCriteria), true);
    }
    else
    {
        $deviceGate = new DeviceTypeTableGateway($dbAdapter);
        $result = $deviceGate->findAll();
    }

    echo json_encode($result);
}
?>