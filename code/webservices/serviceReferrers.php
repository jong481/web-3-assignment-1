<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    if (isset($_GET['referrerID']))
    {
        $whereCriteria = $_GET['referrerID'];

        $referrerGate = new ReferrersTableGateway($dbAdapter);
        $result = $referrerGate->findRecordsBy("ID", array($whereCriteria), true);
    }
    else
    {
        $referrerGate = new ReferrersTableGateway($dbAdapter);
        $result = $referrerGate->findAll();
    }

    echo json_encode($result);
}
?>