<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    $continentGate = new ContinentsTableGateway($dbAdapter);
    $result = $continentGate->findAll();

    echo json_encode($result);
}
?>