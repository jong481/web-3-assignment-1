<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    if (isset($_GET['browserID']))
    {
        $whereCriteria = $_GET['browserID'];

        $browserGate = new BrowserTableGateway($dbAdapter);
        $result = $browserGate->findRecordsBy("id", array($whereCriteria), true);
    }  
    else
    {
        $browserGate = new BrowserTableGateway($dbAdapter);
        $result = $browserGate->findAll();
    }
    
    echo json_encode($result);
}
?>