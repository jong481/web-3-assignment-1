<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    $whereCriteria = ' WHERE ';
    $whereClause = '';
    
    if (isset($_GET['countryName']))
    {
        $whereClause = $whereClause . "country_code='" . $_GET['countryName'] . "' AND ";
    }
    if (isset($_GET['deviceType']))
    {
        $whereClause = $whereClause . "device_type_id='" . $_GET['deviceType'] . "' AND ";
    }
    if (isset($_GET['deviceBrand']))
    {
        $whereClause = $whereClause . "device_brand_id='" . $_GET['deviceBrand'] . "' AND ";
    }
    if (isset($_GET['browserName']))
    {
        $whereClause = $whereClause . "browser_id='" . $_GET['browserName'] . "' AND ";
    }
    if (isset($_GET['referrerName']))
    {
        $whereClause = $whereClause . "referrer_id='" . $_GET['referrerName'] . "' AND ";
    }
    if (isset($_GET['osName']))
    {
        $whereClause = $whereClause . "os_id='" . $_GET['osName'] . "' AND ";
    }
    
    if ($whereClause != '')
    {
        $whereClause = substr($whereClause, 0, -4);
        $whereClause = $whereCriteria . $whereClause;
    }
    
    $visitGate = new VisitsTableGateway($dbAdapter);
    $result = $visitGate->findFirst100Sorted($whereClause, '');

    echo json_encode($result);
}
?>