<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);
function outputJSON($dbAdapter)
{
    if (isset($_GET['general']))
    {
        $whereCriteria = $_GET['general'];
        if ($whereCriteria == 'count_all')
        {
            $visitGate = new VisitsTableGateway($dbAdapter);
            $result = $visitGate->getTableCount();
        }
        else if ($whereCriteria == 'monthCountryCount')
        {
            $month = $_GET['month'];
            $country_code = $_GET['country_code'];
            
            $visitGate = new VisitsTableGateway($dbAdapter);
            $result = $visitGate->findCountPerMonthCountry($month, $country_code);
        }
        else if ($whereCriteria == 'browserCount')
        {
            $browser = $_GET['browser_id'];
            
            $visitGate = new VisitsTableGateway($dbAdapter);
            $result = $visitGate->findCountPerBrowser($browser);
        }
        else if ($whereCriteria == 'brandCount')
        {
            $brand = $_GET['brand_id'];
            
            $visitGate = new VisitsTableGateway($dbAdapter);
            $result = $visitGate->findCountPerBrand($brand);
        }
        else if ($whereCriteria == 'countryCount')
        {
            $country = $_GET['country_id'];
            
            $visitGate = new VisitsTableGateway($dbAdapter);
            $result = $visitGate->findCountPerCountry($country);
        }
    }
    else if (isset($_GET['country_code']))
    {
        $whereCriteria = $_GET['country_code'];

        $visitGate = new VisitsTableGateway($dbAdapter);
        $result = $visitGate->findRecordsBy("country_code", array($whereCriteria), true);
    }
    else if (isset($_GET['browser_id']))
    {
        $whereCriteria = $_GET['browser_id'];

        $visitGate = new VisitsTableGateway($dbAdapter);
        $result = $visitGate->findRecordsBy("browser_id", array($whereCriteria), true);
    }   
    else if (isset($_GET['device_brand_id']))
    {
        $whereCriteria = $_GET['device_brand_id'];

        $visitGate = new VisitsTableGateway($dbAdapter);
        $result = $visitGate->findRecordsBy("device_brand_id", array($whereCriteria), true);
    }
    else if (isset($_GET['rowID']))
    {
        $whereCriteria = $_GET['rowID'];

        $visitGate = new VisitsTableGateway($dbAdapter);
        $result = $visitGate->findRecordsBy("id", array($whereCriteria), true);
    }
    else if (isset($_GET['month']))
    {
        $whereCriteria = $_GET['month'];

        $visitGate = new VisitsTableGateway($dbAdapter);
        $result = $visitGate->find2016MonthRecords($whereCriteria);
    }
    
    echo json_encode($result);
}

?>