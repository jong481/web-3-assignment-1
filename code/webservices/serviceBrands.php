<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    if (isset($_GET['deviceBrandID']))
    {
        $whereCriteria = $_GET['deviceBrandID'];

        $brandGate = new DeviceBrandTableGateway($dbAdapter);
        $result = $brandGate->findRecordsBy("ID", array($whereCriteria), true);
    }
    else
    {
        $brandGate = new DeviceBrandTableGateway($dbAdapter);
        $result = $brandGate->findAll();
    }

    echo json_encode($result);
}
?>