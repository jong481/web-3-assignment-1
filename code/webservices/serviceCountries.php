<?php
require_once('../lib/helpers/visits-setup-services.inc.php');

header('Content-type: application/json');
header("Access-Control-Allow-Origin: *");

outputJSON($dbAdapter);

function outputJSON($dbAdapter)
{
    if (isset($_GET['continent']))
    {
        $whereCriteria = $_GET['continent'];

        $visitGate = new CountriesTableGateway($dbAdapter);
        $result = $visitGate->findRecordsBy("Continent", array($whereCriteria), true);
    }   
    else if (isset($_GET['general']))
    {
        $whereCriteria = $_GET['general'];
        if ($whereCriteria == 'return_all')
        {
            $visitGate = new CountriesTableGateway($dbAdapter);
            $result = $visitGate->findAllSorted('asc');
        }
        else if ($whereCriteria == 'top10')
        {
            $visitGate = new CountriesTableGateway($dbAdapter);
            $result = $visitGate->getTop10Visits();
        }
    }
    else if (isset($_GET['ISO']))
    {
        $whereCriteria = $_GET['ISO'];

        $visitGate = new CountriesTableGateway($dbAdapter);
        $result = $visitGate->findRecordsBy("ISO", array($whereCriteria), true);
    }
    
    echo json_encode($result);
}
?>