<?php
/*
   Represents a single row for the Continents table. 
   
   This a concrete implementation of the Domain Model pattern.
 */
class Continents extends DomainObject implements JsonSerializable
{  
   
   static function getFieldNames() {
      return array('ContinentCode','ContinentName', 'GeoNameId');
   }

   public function __construct(array $data, $generateExc)
   {
      parent::__construct($data, $generateExc);
   }
   
   public function jsonSerialize() {
      return ['id' => $this->ContinentCode, 'name' => $this->ContinentName, 'value' => $this->ContinentCode];
   }
   
   // implement any setters that need input checking/validation
}

?>