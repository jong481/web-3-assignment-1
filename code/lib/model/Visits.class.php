<?php
/*
   Represents a single row for the Visits table. 
   
   This a concrete implementation of the Domain Model pattern.
 */
class Visits extends DomainObject implements JsonSerializable
{  
   
   static function getFieldNames() {
      return array('id','ip_address','country_code','visit_date','visit_time','device_type_id','device_brand_id','browser_id','referrer_id','os_id');
   }

   public function __construct(array $data, $generateExc)
   {
      parent::__construct($data, $generateExc);
   }
   
   public function jsonSerialize() {
      return ['id' => $this->id, 'ip_address' => $this->ip_address, 'visit_date' => $this->visit_date, 'visit_time' => $this->visit_time, 'referrer_id' => $this->referrer_id, 'os_id' => $this->os_id, 'country_code' => $this->country_code, 'device_brand_id' => $this->device_brand_id, 'device_type_id' => $this->device_type_id, 'browser_id' => $this->browser_id];
   }
   
   // implement any setters that need input checking/validation
}

?>