<?php
/*
  Table Data Gateway for the Countries table.
 */
class CountriesTableGateway extends TableDataGateway
{    
   public function __construct($dbAdapter) 
   {
      parent::__construct($dbAdapter);
   }
  
   protected function getDomainObjectClassName()  
   {
      return "Countries";
   } 
   protected function getTableName()
   {
      return "countries";
   }
   protected function getOrderFields() 
   {
      return 'CountryName';
   }
  
   protected function getPrimaryKeyName() 
   {
      return "ISO";
   }
   
   public function getTop10Visits()
   {
      $sql = "SELECT ISO, CountryName from countries WHERE ISO IN (SELECT country_code FROM (SELECT country_code, count(*) visitCount FROM visits GROUP BY country_code ORDER BY visitCount DESC LIMIT 10) q1)";
   
      $results = $this->dbAdapter->fetchAsArray($sql);
      if (is_null($results))
          return $results;
      else         
          return $this->convertRecordsToObjects($results);
   }
}

?>