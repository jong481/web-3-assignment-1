<?php
/*
  Table Data Gateway for the Visits table.
 */
class VisitsTableGateway extends TableDataGateway
{    
   public function __construct($dbAdapter) 
   {
      parent::__construct($dbAdapter);
   }
  
   protected function getDomainObjectClassName()  
   {
      return "Visits";
   } 
   protected function getTableName()
   {
      return "visits";
   }
   protected function getOrderFields() 
   {
      return 'id';
   }
   
   protected function getOrderFieldsVisitBrowser()
   {
      return 'visit_date, visit_time';
   }
  
   protected function getPrimaryKeyName() {
      return "id";
   }
   
   protected function getBrowserKeyName() {
      return "browser_id";
   }

   public function findFirst200Sorted($ascending)
   {
      $sql = $this->getSelectStatement() . ' ORDER BY ' . $this->getOrderFields();
      if (! $ascending) {
         $sql .= " DESC";  
      }
      $sql .= " LIMIT 0, 200";
      return $this->convertRecordsToObjects($this->dbAdapter->fetchAsArray($sql));
   }
   
   public function findByCountryCode($country_code)
   {
      $sql = $this->getSelectStatement() . ' WHERE ' . $this->getPrimaryKeyName() . '=:country_code';
      $results = $this->dbAdapter->fetchRow($sql, Array(':country_code' => $country_code));
       
      if (is_null($results))
          return $results;
      else
        return $this->convertRowToObject($results);
   }   
   
   public function findCountByBrowser($browser_id)
   {
      $sql = $this->getCountSelectStatement() . ' WHERE browser_id =' . $browser_id;
      $results = $this->dbAdapter->fetchRow($sql, Array(':browser_id' => $browser_id));
      
      return $this->convertRowToObject($results);
   }
   
   public function findTotalVisitCount()
   {
      $sql = $this->getCountSelectStatement();
      $results = $this->dbAdapter->fetchRow($sql, Array(null));
      
      return $this->convertRowToObject($results);
   }
   
   public function countRecordsBy($whereClause, $parameterValues=array(), $sortFields=null)
   {
      $sql = 'SELECT COUNT(id) AS\'total\', device_brand_id FROM visits';
      $sql .= ' WHERE ' . $whereClause;
      
      
      if (count($parameterValues) > 1){
         for ($i = 0; $i < count($parameterValues); $i++){
            $sql .=  " like  '". $parameterValues[$i] . "'";
            if($i != (count($parameterValues) - 1)){
               $sql .= " or " . $whereClause;
               }
         }
         $sql .= ";";
      }
      else{
         $sql .=  " like  '". $parameterValues[0] . "' ;";
      }
      // add sort order if required
      if (! is_null($sortFields)) {
         $sql .= ' ORDER BY ' . $sortFields;
      }
      $results = $this->dbAdapter->fetchRow($sql);
      if (is_null($results))
          return $results;
      else         
          return $this->convertRowToObject($results);
   }
   
   public function filterByDate($firstDate, $secondDate)
   {
      $sql = 'SELECT visit_date FROM visits WHERE visit_date BETWEEN ' .
      $firstDate. ' AND ' . $secondDate;
   }
   
   public function findFirst100Sorted($whereClause, $ascending)
   {
      $sql = $this->getSelectStatement() . $whereClause . ' ORDER BY ' . $this->getOrderFieldsVisitBrowser();
      if (! $ascending) {
         $sql .= " DESC";  
      }
      $sql .= " LIMIT 0, 100";
      return $this->convertRecordsToObjects($this->dbAdapter->fetchAsArray($sql));
   }
   
   public function find2016MonthRecords($whereClause)
   {
      $sql = $this->getSelectStatement() .  " WHERE (visit_date BETWEEN '2016-" . $whereClause . "-01 00:00:00' AND '2016-" . $whereClause . "-31 00:00:00') order by visit_date asc";
      
      $results = $this->dbAdapter->fetchAsArray($sql);
      if (is_null($results))
          return $results;
      else         
          return $this->convertRecordsToObjects($results);
   }
   
   public function findCountPerMonthCountry($month, $country_code)
   {
      $sql = "SELECT countries.CountryName as country_code, count(id) as id, month(visit_date) as visit_date FROM visits, countries WHERE visits.country_code = countries.ISO AND country_code = '" . $country_code . "' AND (visit_date BETWEEN '2016-" . $month . "-01 00:00:00' AND '2016-" . $month . "-31 00:00:00') group by country_code";

      $results = $this->dbAdapter->fetchAsArray($sql);
      if (is_null($results))
          return $results;
      else         
          return $this->convertRecordsToObjects($results);
   }
   
   public function findCountPerBrowser($browser_id)
   {
      $sql = "SELECT browsers.name as browser_id, count(visits.id) as id FROM visits, browsers WHERE visits.browser_id = browsers.ID AND visits.browser_id = " . $browser_id . " GROUP BY browsers.name";

      $results = $this->dbAdapter->fetchAsArray($sql);
      if (is_null($results))
          return $results;
      else         
          return $this->convertRecordsToObjects($results);
   }
   
   public function findCountPerBrand($brand_id)
   {
      $sql = "SELECT device_brands.name as device_brand_id, count(visits.id) as id FROM visits, device_brands WHERE visits.device_brand_id = device_brands.ID AND visits.device_brand_id = " . $brand_id . " GROUP BY device_brands.name";

      $results = $this->dbAdapter->fetchAsArray($sql);
      if (is_null($results))
          return $results;
      else         
          return $this->convertRecordsToObjects($results);
   }
   
   public function findCountPerCountry($country_id)
   {
      $sql = "SELECT countries.CountryName as country_code, count(visits.id) as id FROM visits, countries WHERE visits.country_code = countries.ISO AND visits.country_code = '" . $country_id . "' GROUP BY countries.CountryName";

      $results = $this->dbAdapter->fetchAsArray($sql);
      if (is_null($results))
          return $results;
      else         
          return $this->convertRecordsToObjects($results);
   }
}

?>