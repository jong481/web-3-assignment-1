<?php
/*
   Represents a single row for the Operating_Systems table. 
   
   This a concrete implementation of the Domain Model pattern.
 */
class OperatingSystems extends DomainObject implements JsonSerializable
{  
   
   static function getFieldNames() {
      return array('ID','name');
   }

   public function __construct(array $data, $generateExc)
   {
      parent::__construct($data, $generateExc);
   }
   
   public function jsonSerialize() {
      return ['id' => $this->ID, 'value' => $this->name];
   }
   // implement any setters that need input checking/validation
}

?>