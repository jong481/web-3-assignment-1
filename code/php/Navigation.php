<?php

function outputHeader()
{
    echo '<header class="demo-drawer-header">';
    echo '<img src="images/man_256.png" class="demo-avatar">';
    echo '<div class="demo-avatar-dropdown">';
    echo '<div class="mdl-layout-spacer"></div>';
    echo '</div>';
    echo '</header>';
}

function outputNavigation()
{
    echo '<nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">';
    echo '<a class="mdl-navigation__link" href="home.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home - PHP</a>';
    echo '<a class="mdl-navigation__link" href="homejs.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Home - JS</a>';
    echo '<a class="mdl-navigation__link" href="visitbrowser.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Visit Browser</a>';
    echo '<a class="mdl-navigation__link" href="chart.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Charts</a>';
    echo '<a class="mdl-navigation__link" href="about.php"><i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">people</i>About Us</a>';
    echo '<div class="mdl-layout-spacer"></div>';
    echo '</nav>';
}

?>