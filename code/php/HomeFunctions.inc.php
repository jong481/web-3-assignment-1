<?php

/*
    Handles the output of the browser & visit percentage table
*/
function outputBrowserTable($dbAdapter)
{
    $browserGate = new BrowserTableGateway($dbAdapter);
    $result = $browserGate->findAll();
    
    $visitsGate = new VisitsTableGateway($dbAdapter);
    $tableCountResult = $visitsGate->findTotalVisitCount();
    $tableCount = $tableCountResult->countTotal;
    
    foreach ($result as $row)
    {
        $count = $visitsGate->findCountByBrowser($row->id);
        $visitPercentage = round(floatval($count->countTotal) / floatval($tableCount) * 100, 2);

        echo '<tr>';
        echo '<td class="mdl-data-table__cell--non-numeric">'.$row->name.'</td>';
        echo '<td>'.$visitPercentage.'%</td>';
        echo '</tr>';
    }
}

/*
    Handles the output of visits per brand
*/
function listDeviceBrandVisits($dbAdapter)
{
    $deviceGate = new DeviceBrandTableGateway($dbAdapter);
    $deviceBrands = $deviceGate->findAllSorted(true);
   
    echo "<select id=\"deviceBrand\" onchange=\"showCount()\">";
    echo "<option>Select a Brand</option>";
    foreach ($deviceBrands as $deviceBrand)
    {
        $brand = ucfirst($deviceBrand->name);
        $brandID = $deviceBrand->id;
        
        echo "<option value=\"" . $brandID . "\">" . $brand . "</option>";
    }
     echo "</select>";
     echo"<br/>";
     echo "<div id=\"deviceBrandCount\">";
     
     foreach ($deviceBrands as $deviceBrand)
     {
        $brandID = $deviceBrand->id;
        $brandName = $deviceBrand->name;
          
        $visitsGate = new VisitsTableGateway($dbAdapter);
        $visits = $visitsGate->countRecordsBy('device_brand_id', array($brandID), null);
        $count = $visits->total;
          
        echo "<p style=\"display:none\" id=\"deviceBrand".$brandID."\">".$count." visitors using ".$brandName." devices</p>";
     }
     
     echo "</div>";
}

/*
    Lists the continents as dropdown options
*/
function listContinents($dbAdapter)
{
    $continentsGate = new ContinentsTableGateway($dbAdapter);
    $continents = $continentsGate->findAllSorted(true);
   
    echo "<select id=\"continents\" onchange=\"showCountriesTable()\">";
    echo "<option>Select a Continent</option>";
    foreach ($continents as $continent)
    {
        $continentName = ucfirst($continent->ContinentName);
        $continentCode = $continent->ContinentCode;
        echo "<option value=\"" . $continentCode . "\">" . $continentName . "</option>";
    }
    echo "</select>";
}

function outputCountriesTable($continentCode, $dbAdapter)
{
        $countriesGate = new CountriesTableGateway($dbAdapter);
        $countries = $countriesGate->findRecordsBy("Continent", array($continentCode), true);
       
        echo '<table id="table'.$continentCode.'" style="display:none" class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp, countryTable">';
        echo '<thead>';
        echo '<tr>';
        echo '<th class="mdl-data-table__cell--non-numeric">Country</th>';
        echo '<th >Visits</th>';
        echo '<th class="mdl-data-table__cell--non-numeric">Country</th>';
        echo '<th>Visits</th>';
        echo '<tr>';
        echo '</thead>';
        echo '<tbody>';
        $counter = 1;
        foreach($countries as $country)
        {
            $visitsGate = new VisitsTableGateway($dbAdapter);
            $visits = $visitsGate->countRecordsBy('country_code', array($country->ISO), 'ascending');
            $count = $visits->total;    
        
            if($counter % 2 == 0){
                echo '<td class="mdl-data-table__cell--non-numeric">'.$country->CountryName.'</td>';
                echo '<td>'.$count.'</td>';
                echo '</tr>';
            }else{
                echo '<tr>';    
                echo '<td class="mdl-data-table__cell--non-numeric">'.$country->CountryName.'</td>';
                echo '<td>'.$count.'</td>';
            }
            
            $counter++;
        }
        
        echo '</tbody>';
        echo '</table>';
}
    
function generateCountryTableFromContinents($dbAdapter)
{
    $continentsGate = new ContinentsTableGateway($dbAdapter);
    $continents = $continentsGate->findAllSorted(true);
   
    foreach ($continents as $continent)
    {
        $continentCode = $continent->ContinentCode;
        outputCountriesTable($continentCode, $dbAdapter);
    }
}    

?> 