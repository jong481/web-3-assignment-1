<?php
/*
   Use this page to test each one of your table gateway classes.
*/

/*
   To RUN ANY PHP FILE/CODE.  -> click the Run Project Button ^
                              -> Apache & PHP Process will create a new tab below
                              -> click the link it produces and open as Live Preview
*/

require_once('lib/helpers/visits-setup.inc.php');
?>

<html>
<body>
<h1> Table Gateways Tester </h1>

<?php



echo '<hr/>';
echo '<h2>Test BrowserTableGateway</h2>';
echo '<h3>Test findAll()</h3>';
$gate = new BrowserTableGateway($dbAdapter);
$result = $gate->findAll();

foreach ($result as $row)
{
   echo $row->id . " - " . $row->name . "<br/>";
}

echo '<h3>Test findById(3)</h3>';
$result = $gate->findById(3);
echo $result->id . " - " . $result->name;
  


echo '<hr/>';
echo '<h2>Test DeviceBrandTableGateway</h2>';
echo '<h3>Test findAllSorted()</h3>';
$gate = new DeviceBrandTableGateway($dbAdapter);
$result = $gate->findAllSorted(true);
foreach ($result as $row)
{
   echo $row->id . " - " . $row->name . "<br/>";
}

echo '<h3>Test findById(11)</h3>';
$result = $gate->findById(11);
echo $result->id . " - " . $result->name;



//******************************************************************************
//******************************************************************************
//******************************************************************************
//****************************Jan's Tests***************************************
//******************************************************************************
//******************************************************************************



echo '<h1>Jans Test</h1>';



//******************************************************************************
echo '<hr/>';
echo '<h2>Test ContinentsTableGateway</h2>';
echo '<h3>Test findAllSorted()</h3>';
$gate = new ContinentsTableGateway($dbAdapter);
$result = $gate->findAllSorted(true);
foreach ($result as $row)
{
   echo $row->ContinentCode . " - " . $row->ContinentName . "<br/>";
}

echo '<h3>Test findById(NA)</h3>';
$result = $gate->findById('NA');
echo $result->ContinentCode . " - " . $result->ContinentName;
//******************************************************************************


//******************************************************************************
echo '<hr/>';
echo '<h2>Test CountriesTableGateway</h2>';
echo '<h3>Test findAllSorted()</h3>';
$gate = new CountriesTableGateway($dbAdapter);
$result = $gate->findAllSorted(true);
//foreach ($result as $row)
//{
//   echo $row->ISO . " - " . $row->fipsCountryCode . " - " . $row->ISO3 . " - " . $row->ISONumeric . " - " . $row->CountryName . " - " . $row->Capital . " - " . $row->GeoNameID . " - " . $row->Area . " - " . $row->Population . " - " . $row->Continent . " - " . $row->TopLevelDomain . " - " . $row->CurrencyCode . " - " . $row->CurrencyName . " - " . $row->PhoneCountryCode . " - " . $row->Languages . " - " . $row->PostalCodeFormat . " - " . $row->PostalCodeRegex . " - " . $row->Neighbours . " - " . $row->CountryDescription . "<br/>";
//}

echo '<h3>Test findById(CA)</h3>';
$result = $gate->findById('CA');
echo $result->ISO . " - " . $result->CountryName;
//******************************************************************************

//******************************************************************************
echo '<hr/>';
echo '<h2>Test DeviceTypeTableGateway</h2>';
echo '<h3>Test findAllSorted()</h3>';
$gate = new DeviceTypeTableGateway($dbAdapter);
$result = $gate->findAllSorted(true);
foreach ($result as $row)
{
   echo $row->ID . " - " . $row->name . "<br/>";
}

echo '<h3>Test findById(3)</h3>';
$result = $gate->findById(3);
echo $result->ID . " - " . $result->name;
//******************************************************************************

//******************************************************************************
echo '<hr/>';
echo '<h2>Test OperatingSystemTableGateway</h2>';
echo '<h3>Test findAllSorted()</h3>';
$gate = new OperatingSystemsTableGateway($dbAdapter);
$result = $gate->findAllSorted(true);
foreach ($result as $row)
{
   echo $row->ID . " - " . $row->name . "<br/>";
}

echo '<h3>Test findById(10)</h3>';
$result = $gate->findById(10);
echo $result->ID . " - " . $result->name;
//******************************************************************************

//******************************************************************************
echo '<hr/>';
echo '<h2>Test ReferrersTableGateway</h2>';
echo '<h3>Test findAllSorted()</h3>';
$gate = new ReferrersTableGateway($dbAdapter);
$result = $gate->findAllSorted(true);
//foreach ($result as $row)
//{
//   echo $row->id . " - " . $row->name . "<br/>";
//}

echo '<h3>Test findById(6)</h3>';
$result = $gate->findById(6);
echo $result->id . " - " . $result->name;
//******************************************************************************

//******************************************************************************
echo '<hr/>';
echo '<h2>Test VisitsTableGateway</h2>';
echo '<h3>Test findFirst200Sorted()</h3>';
$gate = new VisitsTableGateway($dbAdapter);
$result = $gate->findFirst200Sorted(true);
//foreach ($result as $row)
//{
//  echo $row->id . " - " . $row->ip_address . " - " . $row->country_code . " - " . $row->visit_date . " - " . $row->visit_time . " - " . $row->device_type_id . " - " . $row->device_brand_id . " - " . $row->browser_id . " - " . $row->referrer_id . " - " . $row->os_id . "<br/>";
//}

echo '<h3>Test findById(909)</h3>';
$result = $gate->findById(909);
// echo $result->id . " - " . $result->ip_address . " - " . $result->country_code . " - " . $result->visit_date . " - " . $result->visit_time . " - " . $result->device_type_id . " - " . $result->device_brand_id . " - " . $result->bresultser_id . " - " . $result->referrer_id . " - " . $result->os_id;
//******************************************************************************




echo '<br/><br/><h3>Basic Data Access Implementation Completed</h3>';




echo '<br/><h1>CUSTOM FUCNTIONS TEST</h1><br/>';

echo '<hr/>';
echo '<h2>Test VisitsTableGateway</h2>';
echo '<h3>Test CustomFindBy1Critetia</h3>';
$gate = new VisitsTableGateway($dbAdapter);
$result = $gate->findRecordsBy('country_code', array('vn'), null);
//foreach ($result as $row)
//{
//  echo $row->id . " - " . $row->ip_address . " - " . $row->country_code . " - " . $row->visit_date . " - " . $row->visit_time . " - " . $row->device_type_id . " - " . $row->device_brand_id . " - " . $row->browser_id . " - " . $row->referrer_id . " - " . $row->os_id . "<br/>";
//}

echo '<hr/>';
echo '<h2>Test VisitsTableGateway</h2>';
echo '<h3>Test CustomFindByMultipleCritetia</h3>';
$gate = new VisitsTableGateway($dbAdapter);
$result = $gate->findRecordsBy('country_code', array('vn', 'ca', 'us'), null);
//foreach ($result as $row)
//{
//  echo $row->id . " - " . $row->ip_address . " - " . $row->country_code . " - " . $row->visit_date . " - " . $row->visit_time . " - " . $row->device_type_id . " - " . $row->device_brand_id . " - " . $row->browser_id . " - " . $row->referrer_id . " - " . $row->os_id . "<br/>";
//}


echo '<hr/>';
echo '<h2>Test VisitsTableGateway</h2>';
echo '<h3>Test CustomCountByMultipleCritetia</h3>';
$gate = new VisitsTableGateway($dbAdapter);
$result = $gate->countRecordsBy('country_code', array('vn', 'ca', 'us'), null);

echo var_dump($result);
echo $result->total;

// all done close connection
$dbAdapter->closeConnection();

?>