<?php
require_once('code/php/Navigation.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="A front-end template that helps you build fast, modern mobile web apps.">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Assignment 1x</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="css/material.cyan-light_blue.min.css">
    <link rel="stylesheet" href="css/material.css">
  </head>
  <body>
    <div class="demo-layout mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
      <div class="demo-drawer mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50">
        <?php
          outputHeader();
          outputNavigation();
        ?>
      </div>
      <main class="mdl-layout__content mdl-color--grey-100">
        <div class="mdl-grid demo-content">
          <div class="demo-graphs mdl-shadow--2dp mdl-color--white mdl-cell mdl-cell--12-col">
            <h2 class="mdl-card__title-text">About Us</h2>
            Members:</br>
            <div class="mdl-grid demo-content">
                <div class="demo-options mdl-card mdl-color--deep-purple-500 mdl-shadow--2dp mdl-cell mdl-cell--2-col-desktop">
                  <div class="mdl-card__supporting-text mdl-color-text--blue-grey-50">
                    <h3>Jad Dizon</h3>
                  </div>
                </div>
                <div class="demo-options mdl-card mdl-color--deep-purple-500 mdl-shadow--2dp mdl-cell mdl-cell--2-col-desktop">
                  <div class="mdl-card__supporting-text mdl-color-text--blue-grey-50">
                    <h3>Jan Genese</h3>
                  </div>
                </div>
                <div class="demo-options mdl-card mdl-color--deep-purple-500 mdl-shadow--2dp mdl-cell mdl-cell--2-col-desktop">
                  <div class="mdl-card__supporting-text mdl-color-text--blue-grey-50">
                    <h3>Jed Ong</h3>
                  </div>
                </div>
                <div class="demo-options mdl-card mdl-color--deep-purple-500 mdl-shadow--2dp mdl-cell mdl-cell--2-col-desktop">
                  <div class="mdl-card__supporting-text mdl-color-text--blue-grey-50">
                    <h3>Daniel Truong</h3>
                  </div>
                </div>
            </div>
            Code Repository URL: <a href="https://bitbucket.org/jong481/web-3-assignment-1/overview">BitBucket</a></br>
            Resources Used:</br>
            <ul>
              <li>Material Design: <a href ="https://www.getmdl.io">Material Design Lite</a></li>
              <li>Dashboard Template: <a href="http://www.getmdl.io/templates/dashboard/index.html">Material Design Lite - Dashboard Template</a></li>
              <li>Textbox AutoComplete: <a href="http://easyautocomplete.com">EasyAutoComplete Plugin</a></li>
              <li>Table Modal: <a href="http://www.w3schools.com/howto/howto_css_modals.asp">Modal Design</a></li>
            </ul>
          </div>
        </div>
      </main>
    </div>
    <script src="https://code.getmdl.io/1.1.1/material.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  </body>
</html>